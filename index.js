// [SECTION] JavaScript Synchronous vs Asynchronous

// JavaScript is by default synchronous meaning that it only executes one statement at a time.

console.log("Hello World");
console.log("Good bye");
// Code blocking - waiting for the specific statement to finish before executing the next statement
// for (let i = 0; i <= 1500; i++){
// 	console.log(i);
// }

console.log("Hello Again");

// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background.

// [SECTION] Getting all posts

// The Fetch API that allows us to asynchronously request for a resources (data).
	// "fetch()" method in JavaScript is used to request to the server and load information on the webpages.
	// Syntax:
		// fetch("apiURL")

	// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value.
	// A promise may be in one of 3 possible states: fullfilles, rejected or pending.
		/*
			pending: initial states, neither fulfilled nor rejected response.
			fulfilled: operation was completed.
			rejected: operation failed.

			Syntax:
			fetch("apiURL")
			.then(response => {responseMethod/code block})

		*/

	// make sure that fetch doesnt have a ";"
	fetch("https://jsonplaceholder.typicode.com/posts")
	// The ".then()" method captures the response object and returns another promise which will be either "resolved" or "rejected" 
	//.then(response => console.log(response.status));

	// "json()" method will convert JSON format to JavaScript object
	.then(response => response.json())
	// we can now manipulate or use the converted response in our code.
	.then(response => console.log(response));
	// We can use array methods to display each post title.
	// .then(json =>{
	// 	json.forEach(post => console.log(post.title))
	// }) - to access each title

	// The "async" and "await" keyword to achieve asynchronous code.

	async function fetchData(){
		let result = await(fetch("https://jsonplaceholder.typicode.com/posts"));
		// Returned the "response" object
		console.log(result);

		let json = await result.json();
		console.log(json);
	}

	fetchData();

	// [SECTION] Getting a specific post
	fetch("https://jsonplaceholder.typicode.com/posts/1")
	.then(response => response.json())
	.then(json => console.log(json));

	// [SECTION] Creating a post
	/*
		Syntax:
			fetch("apiURL", options)
			.then((response) => {})
			.then((response) => {})
	*/

	fetch("https://jsonplaceholder.typicode.com/posts", 
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					title: "New Post",
					body: "Hello World!",
					userId: 1
				})
			}
		)

	.then(response => response.json())
	.then(json => console.log(json));

	// [SECTION] Updating a post
	fetch("https://jsonplaceholder.typicode.com/posts/1",{
		method: "PUT",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			title: "updated post",
			body: "Hello again",
			userId: 1
		})
	})
	.then(response => response.json())
	.then(json => console.log(json));

	// [SECTION] Updating a post using PATCH Method
	// PUT vs PATCH
		// PATCH is used to update a single/several properties
		// PUT is used to update the whole document/object
	fetch("https://jsonplaceholder.typicode.com/posts/1",{
		method: "PATCH",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			title: "Corrected post title"	
		})
	})
	.then(response => response.json())
	.then(json => console.log(json));

	// [SECTION] Delete a post
	fetch("https://jsonplaceholder.typicode.com/posts/1", {method: "DELETE"})

	.then(response => response.json())
	.then(json => console.log(json));